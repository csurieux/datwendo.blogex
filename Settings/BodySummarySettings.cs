﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.ContentManagement.MetaData.Models;
using Orchard.ContentManagement.ViewModels;

namespace Datwendo.BlogEx.Settings {
    public class BodySummaryPartSettings {
        public const string FlavorDefaultDefault = "html";
        private string _flavorDefault;

        [DataType("Flavor")]
        public string FlavorDefault {
            get { return !string.IsNullOrWhiteSpace(_flavorDefault)
                           ? _flavorDefault
                           : FlavorDefaultDefault; }
            set { _flavorDefault = value; }
        }
    }

    public class BodySummaryTypePartSettings {
        [DataType("Flavor")]
        public string Flavor { get; set; }
    }

    public class BodySettingsHooks : ContentDefinitionEditorEventsBase {
        public override IEnumerable<TemplateViewModel> TypePartEditor(ContentTypePartDefinition definition) {
            if (definition.PartDefinition.Name != "BodySummaryPart")
                yield break;

            var model = definition.Settings.GetModel<BodySummaryTypePartSettings>();

            if (string.IsNullOrWhiteSpace(model.Flavor)) {
                var partModel = definition.PartDefinition.Settings.GetModel<BodySummaryPartSettings>();
                model.Flavor = partModel.FlavorDefault;
            }

            yield return DefinitionTemplate(model);
        }

        public override IEnumerable<TemplateViewModel> PartEditor(ContentPartDefinition definition) {
            if (definition.Name != "BodySummaryPart")
                yield break;

            var model = definition.Settings.GetModel<BodySummaryPartSettings>();
            yield return DefinitionTemplate(model);
        }

        public override IEnumerable<TemplateViewModel> TypePartEditorUpdate(ContentTypePartDefinitionBuilder builder, IUpdateModel updateModel) {
            if (builder.Name != "BodySummaryPart")
                yield break;

            var model = new BodySummaryTypePartSettings();
            updateModel.TryUpdateModel(model, "BodySummaryTypePartSettings", null, null);
            builder.WithSetting("BodySummaryTypePartSettings.Flavor", !string.IsNullOrWhiteSpace(model.Flavor) ? model.Flavor : null);
            yield return DefinitionTemplate(model);
        }

        public override IEnumerable<TemplateViewModel> PartEditorUpdate(ContentPartDefinitionBuilder builder, IUpdateModel updateModel) {
            if (builder.Name != "BodySummaryPart")
                yield break;

            var model = new BodySummaryPartSettings();
            updateModel.TryUpdateModel(model, "BodySummaryPartSettings", null, null);
            builder.WithSetting("BodySummaryPartSettings.FlavorDefault", !string.IsNullOrWhiteSpace(model.FlavorDefault) ? model.FlavorDefault : null);
            yield return DefinitionTemplate(model);
        }
    }
}
