using System;
using System.Web;
using System.Web.Mvc;
using Orchard.DisplayManagement;
using Orchard.DisplayManagement.Descriptors;
using Orchard.Localization;
using Orchard.Localization.Services;
using System.Web.Routing;
using Orchard;
using Orchard.Environment;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Orchard.Utility.Extensions;

namespace Datwendo.BlogEx {
    public class Shapes : IShapeTableProvider {
        private readonly IDateLocalizationServices _dateLocalizationServices;
        private readonly IDateTimeFormatProvider _dateTimeLocalization;
        private readonly Work<WorkContext> _workContext;
        private readonly Work<IShapeFactory> _shapeFactory;


        public Localizer T { get; set; }
        
        public Shapes(  IDateLocalizationServices dateLocalizationServices
                        , Work<WorkContext> workContext
                        , Work<IShapeFactory> shapeFactory
                        , IDateTimeFormatProvider dateTimeLocalization
                        ) {
            _workContext = workContext;
            _shapeFactory = shapeFactory;
            _dateLocalizationServices = dateLocalizationServices;
            _dateTimeLocalization = dateTimeLocalization;
            T = NullLocalizer.Instance;
        }

        public dynamic New { get { return _shapeFactory.Value; } }

        public void Discover(ShapeTableBuilder builder) {
            builder.Describe("BodySummary_Editor")
                .OnDisplaying(displaying => {
                    string flavor = displaying.Shape.EditorFlavor;
                    displaying.ShapeMetadata.Alternates.Add("BodySummary_Editor__" + flavor);
                });
            builder.Describe("BList")
                            .OnCreated(created => {
                                var list = created.Shape;
                                list.Tag = "ul";
                                list.ItemClasses = new List<string>();
                                list.ItemAttributes = new Dictionary<string, string>();
                            });
            builder.Describe("BPager_Links")
                .OnDisplaying(displaying => {
                    var pager = displaying.Shape;
                    string pagerId = pager.PagerId;
                    if (!String.IsNullOrWhiteSpace(pagerId))
                        displaying.Shape.Metadata.Alternates.Add("BPager_Links__" + EncodeAlternateElement(pagerId));
                });
        }
        private string EncodeAlternateElement(string alternateElement) {
            return alternateElement.Replace("-", "__").Replace(".", "_");
        }

        [Shape]
        public IHtmlString ShortPublishedDate(dynamic Display,DateTime? publisheddateTimeUtc) {
            if (!publisheddateTimeUtc.HasValue)
            {
                return T("Draft");
            }
            return new MvcHtmlString(_dateLocalizationServices.ConvertToLocalizedString(publisheddateTimeUtc, _dateTimeLocalization.LongDateFormat));
        }

        [Shape]
        public void BList(
            dynamic Display,
            TextWriter Output,
            IEnumerable<dynamic> Items,
            string Tag,
            string Id,
            IEnumerable<string> Classes,
            IDictionary<string, string> Attributes,
            string ItemTag,
            IEnumerable<string> ItemClasses,
            IDictionary<string, string> ItemAttributes) {

            if (Items == null)
                return;

            // prevent multiple enumerations
            var items = Items.ToList();

            var count = items.Count();
            if (count < 1)
                return;

            var listTag = new TagBuilder("ul");
            listTag.AddCssClass("pagination");
            if (Classes != null && Classes.Any()) {
                foreach (var cls in Classes)
                    listTag.AddCssClass(cls);
            }


            Output.Write(listTag.ToString(TagRenderMode.StartTag));
            

            foreach (var item in items) {
                var itemOutput = Display(item).ToHtmlString();

                if (!String.IsNullOrWhiteSpace(itemOutput)) {
                    Output.Write(itemOutput);
                }
            }
            Output.WriteLine(listTag.ToString(TagRenderMode.EndTag));
        }

        [Shape]
        public IHtmlString BPager_Links(dynamic Shape, dynamic Display,
            HtmlHelper Html,
            int Page,
            int PageSize,
            double TotalItemCount,
            int? Quantity,
            object FirstText,
            object PreviousText,
            object NextText,
            object LastText,
            object GapText,
            string PagerId) {

            var currentPage = Page;
            if (currentPage < 1)
                currentPage = 1;

            var pageSize = PageSize;

            var numberOfPagesToShow = Quantity ?? 0;
            if (Quantity == null || Quantity < 0)
                numberOfPagesToShow = 7;

            var totalPageCount = pageSize > 0 ? (int)Math.Ceiling(TotalItemCount / pageSize) : 1;

            var firstText = FirstText ?? T("&laquo;&laquo;");
            var previousText = PreviousText ?? T("&laquo;");
            var nextText = NextText ?? T("&raquo;");
            var lastText = LastText ?? T("&raquo;&raquo;");
            var gapText = GapText ?? T("...");

            var routeData = new RouteValueDictionary(Html.ViewContext.RouteData.Values);
            var queryString = _workContext.Value.HttpContext.Request.QueryString;
            if (queryString != null) {
                foreach (var key in from string key in queryString.Keys where key != null && !routeData.ContainsKey(key) let value = queryString[key] select key) {
                    routeData[key] = queryString[key];
                }
            }

            // specific cross-requests route data can be passed to the shape directly (e.g., Orchard.Users)
            var shapeRoute = (object)Shape.RouteData;

            if (shapeRoute != null) {
                var shapeRouteData = shapeRoute as RouteValueDictionary;
                if (shapeRouteData == null) {
                    var route = shapeRoute as RouteData;
                    if (route != null) {
                        shapeRouteData = (route).Values;
                    }
                }

                if (shapeRouteData != null) {
                    foreach (var rd in shapeRouteData) {
                        routeData[rd.Key] = rd.Value;
                    }
                }
            }

            // HACK: MVC 3 is adding a specific value in System.Web.Mvc.Html.ChildActionExtensions.ActionHelper
            // when a content item is set as home page, it is rendered by using Html.RenderAction, and the routeData is altered
            // This code removes this extra route value
            var removedKeys = routeData.Keys.Where(key => routeData[key] is DictionaryValueProvider<object>).ToList();
            foreach (var key in removedKeys) {
                routeData.Remove(key);
            }

            int firstPage = Math.Max(1, Page - (numberOfPagesToShow / 2));
            int lastPage = Math.Min(totalPageCount, Page + (int)(numberOfPagesToShow / 2));

            var pageKey = String.IsNullOrEmpty(PagerId) ? "page" : PagerId;

            //Shape.Classes.Add("pager");
            Shape.Metadata.Alternates.Clear();
            Shape.Metadata.Type = "BList";

            // first and previous pages
            if (Page > 1) {
                if (routeData.ContainsKey(pageKey)) {
                    routeData.Remove(pageKey); // to keep from having "page=1" in the query string
                }
                // first
                Shape.Add(New.Pager_First(Value: firstText, RouteValues: new RouteValueDictionary(routeData), Pager: Shape));

                // previous
                if (currentPage > 2) { // also to keep from having "page=1" in the query string
                    routeData[pageKey] = currentPage - 1;
                }
                Shape.Add(New.Pager_Previous(Value: previousText, RouteValues: new RouteValueDictionary(routeData), Pager: Shape));
            }
            
            // gap at the beginning of the pager
            if (firstPage > 1 && numberOfPagesToShow > 0) {
                Shape.Add(New.Pager_Gap(Value: gapText, Pager: Shape));
            }
            
            // page numbers
            if (numberOfPagesToShow > 0 && lastPage > 1) {
                for (var p = firstPage; p <= lastPage; p++) {
                    if (p == currentPage) {
                        Shape.Add(New.Pager_Page(Value: p, RouteValues: new RouteValueDictionary(routeData), Pager: Shape, IsCurrent: true));
                    }
                    else {
                        if (p == 1)
                            routeData.Remove(pageKey);
                        else
                            routeData[pageKey] = p;
                        Shape.Add(New.Pager_Page(Value: p, RouteValues: new RouteValueDictionary(routeData), Pager: Shape, IsCurrent: false));
                    }
                }
            }
            
            // gap at the end of the pager
            if (lastPage < totalPageCount && numberOfPagesToShow > 0) {
                Shape.Add(New.Pager_Gap(Value: gapText, Pager: Shape));
            }
            
            // next and last pages
            if (Page < totalPageCount) {
                // next
                routeData[pageKey] = Page + 1;
                Shape.Add(New.Pager_Next(Value: nextText, RouteValues: new RouteValueDictionary(routeData), Pager: Shape));
                // last
                routeData[pageKey] = totalPageCount;
                Shape.Add(New.Pager_Last(Value: lastText, RouteValues: new RouteValueDictionary(routeData), Pager: Shape));
            }

            return Display(Shape);
        }

        // Private in CoreShapes ...
        static TagBuilder GetTagBuilder(string tagName, string id, IEnumerable<string> classes, IDictionary<string, string> attributes) {
            var tagBuilder = new TagBuilder(tagName);
            tagBuilder.MergeAttributes(attributes, false);
            foreach (var cssClass in classes ?? Enumerable.Empty<string>())
                tagBuilder.AddCssClass(cssClass);
            if (!string.IsNullOrWhiteSpace(id))
                tagBuilder.GenerateId(id);
            return tagBuilder;
        }

        // Utility, thanks to DS & BL (http://weblogs.asp.net/bleroy/so-you-don-t-want-to-use-placement-info)
        public static dynamic Find(dynamic shape, string shapeType, string shapeName = null) {
            if (shape.Metadata.Type == shapeType && (shapeName == null || shape.Name == shapeName)) {
                return shape;
            }
            foreach (var item in shape.Items) {
                var result = Find(item, shapeType, shapeName);
                if (result != null) return result;
            }
            return null;
        }
    }
}
