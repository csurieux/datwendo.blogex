﻿using Orchard.ContentManagement.MetaData;
using Orchard.Data.Migration;
using Orchard.Core.Contents.Extensions;
using Datwendo.BlogEx.Models;

namespace Datwendo.BlogEx
{
    public class Migrations : DataMigrationImpl {
 
        public int Create() {
            ContentDefinitionManager.AlterPartDefinition(typeof(SummaryWidgetPart).Name,
                            builder => builder
                                .Attachable()
                                .WithDescription("Blog Summary Widget Part"));

            ContentDefinitionManager.AlterTypeDefinition("SummaryWidget",
                cfg => cfg
                    .WithPart("SummaryWidgetPart")
                    .WithPart("CommonPart")
                    .WithPart("WidgetPart")
                    .WithSetting("Stereotype", "Widget")
                    );


            SchemaBuilder.CreateTable("BodySummaryPartRecord",
                table => table
                    .ContentPartVersionRecord()
                    .Column<string>("Text", column => column.Unlimited())
                    .Column<string>("Format")
                );

            ContentDefinitionManager.AlterPartDefinition("BodySummaryPart", builder => builder
                .Attachable()
                .WithDescription("Allows the editing of text using an editor provided by the configured flavor (e.g. html, text, markdown)."));

            ContentDefinitionManager.AlterTypeDefinition("BlogPost",
                cfg => cfg
                    .WithPart("BlogPost")
                    .WithPart("BodySummaryPart")
                );

            ContentDefinitionManager.AlterPartDefinition("BlogPost", //same name as contentype to attach them directly to contentitem
                builder => builder
                    .WithField("Sujet", fcfg => fcfg
                        .OfType("TaxonomyField")
                        .WithDisplayName("Sujet")
                        .WithSetting("TaxonomyFieldSettings.Taxonomy", "Sujet")
                        .WithSetting("TaxonomyFieldSettings.LeavesOnly", "true")
                        .WithSetting("TaxonomyFieldSettings.SingleChoice", "false")
                        .WithSetting("TaxonomyFieldSettings.Hint", "Préciser le contenu grâce à une taxonomie")
                        .WithSetting("TaxonomyFieldSettings.Required", "true"))
                    .WithField("Images", f => f
                        .OfType("MediaLibraryPickerField")
                        .WithDisplayName("Images Associés")
                        .WithSetting("MediaLibraryPickerFieldSettings.Hint", "Associer des images à votre blog")
                        .WithSetting("MediaLibraryPickerFieldSettings.Multiple", "true"))
                        );
            ContentDefinitionManager.AlterTypeDefinition("Blog",
                cfg => cfg
                    .WithPart("Blog")
                );

            ContentDefinitionManager.AlterPartDefinition("Blog", //same name as contentype to attach them directly to contentitem
                builder => builder
                    .WithField("Sujet", fcfg => fcfg
                        .OfType("TaxonomyField")
                        .WithDisplayName("Sujet")
                        .WithSetting("TaxonomyFieldSettings.Taxonomy", "Sujet")
                        .WithSetting("TaxonomyFieldSettings.LeavesOnly", "true")
                        .WithSetting("TaxonomyFieldSettings.SingleChoice", "false")
                        .WithSetting("TaxonomyFieldSettings.Hint", "Préciser le contenu grâce à une taxonomie")
                        .WithSetting("TaxonomyFieldSettings.Required", "true"))
                    .WithField("Images", f => f
                        .OfType("MediaLibraryPickerField")
                        .WithDisplayName("Images Associés")
                        .WithSetting("MediaLibraryPickerFieldSettings.Hint", "Associer des images à votre blog")
                        .WithSetting("MediaLibraryPickerFieldSettings.Multiple", "false"))
                        );
            return 3;
        }
        public int UpdateFrom2() {
            ContentDefinitionManager.AlterTypeDefinition("Blog",
                cfg => cfg
                    .WithPart("Blog")
                );

            ContentDefinitionManager.AlterPartDefinition("Blog", //same name as contentype to attach them directly to contentitem
                builder => builder
                    .WithField("Sujet", fcfg => fcfg
                        .OfType("TaxonomyField")
                        .WithDisplayName("Sujet")
                        .WithSetting("TaxonomyFieldSettings.Taxonomy", "Sujet")
                        .WithSetting("TaxonomyFieldSettings.LeavesOnly", "true")
                        .WithSetting("TaxonomyFieldSettings.SingleChoice", "false")
                        .WithSetting("TaxonomyFieldSettings.Hint", "Préciser le contenu grâce à une taxonomie")
                        .WithSetting("TaxonomyFieldSettings.Required", "true"))
                    .WithField("Images", f => f
                        .OfType("MediaLibraryPickerField")
                        .WithDisplayName("Images Associés")
                        .WithSetting("MediaLibraryPickerFieldSettings.Hint", "Associer des images à votre blog")
                        .WithSetting("MediaLibraryPickerFieldSettings.Multiple", "false"))
                        );
        return 3;
        }

    }
}