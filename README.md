# Datwendo.BlogEx module is an Orchard 1.9.x module which adds a dedicated BodySummaryPart in place of the basics substring truncation used on the html code #

01/25/2016 Various fixes 

* Replace all BodyPart Summaries by a dedicated one
* Also adds Taxonomy and ImageLibraryPicker fields in its migration, you may want to comment them. They will be soon placed in a separate feature.
* Version 1.0


** WARNING: this version uses a 3rd parameter in filter.ProcessContent(text, GetFlavor(part),part.ContentItem)), this is in order to be able to process Tokens introduced by a #{ mark in body text, you need to apply this PR before being able to use it https://github.com/OrchardCMS/Orchard/pull/6078, or I will take time to reformat and Sebastien Ross could be ok with it..... 

* Christian Surieux - Datwendo - 2016
* Apacha v2 license