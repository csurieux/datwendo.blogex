﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orchard.Mvc.Html;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.ContentManagement.Drivers;
using Datwendo.BlogEx.Models;
using Datwendo.BlogEx.Settings;
using Datwendo.BlogEx.ViewModels;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard;
using Orchard.ContentManagement.Handlers;
using Datwendo.BlogEx.Services;
using Orchard.Core.Common.Models;
using Orchard.Utility.Extensions;

namespace Datwendo.BlogEx.Drivers {
    public class BodySummaryPartDriver : ContentPartDriver<BodySummaryPart> {
        private readonly IEnumerable<IHtmlFilterEx> _htmlFilters;
        private readonly RequestContext _requestContext;

        private const string TemplateName = "Parts.Common.BodySummary";

        public BodySummaryPartDriver(IOrchardServices services, IEnumerable<IHtmlFilterEx> htmlFilters, RequestContext requestContext) {
            _htmlFilters = htmlFilters;
            Services = services;
            _requestContext = requestContext;
        }

        public IOrchardServices Services { get; set; }

        protected override string Prefix {
            get { return "BodySummary"; }
        }

        protected override DriverResult Display(BodySummaryPart part, string displayType, dynamic shapeHelper) {
            // if bodyPart exists all is done inside it, return null DriverResult
            var body = part.ContentItem.As<BodyPart>();
            return ( body != null ) ? null :ContentShape("Parts_Common_BodySummary",
                             () => {
                                 string txt = part.Text.Trim();
                                 var bodyText = _htmlFilters.Aggregate(txt, (text, filter) => filter.ProcessContent(text, GetFlavor(part),part.ContentItem));
                                 return shapeHelper.Parts_Common_BodySummary(Html: new HtmlString(bodyText));
                             });
        }
        public string Excerpt(string markup, int length)
        {
            return markup.RemoveTags().Ellipsize(length);
        }
        protected override DriverResult Editor(BodySummaryPart part, dynamic shapeHelper) {
            var model = BuildEditorViewModel(part,_requestContext);
            return ContentShape("Parts_Common_BodySummary_Edit",
                                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: model, Prefix: Prefix));
        }

        protected override DriverResult Editor(BodySummaryPart part, IUpdateModel updater, dynamic shapeHelper) {
            var model = BuildEditorViewModel(part, _requestContext);
            updater.TryUpdateModel(model, Prefix, null, null);

            return ContentShape("Parts_Common_BodySummary_Edit", 
                                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: model, Prefix: Prefix));
        }

        protected override void Importing(BodySummaryPart part, ImportContentContext context) {
            // Don't do anything if the tag is not specified.
            if (context.Data.Element(part.PartDefinition.Name) == null) {
                return;
            }

            context.ImportAttribute(part.PartDefinition.Name, "Text", importedText =>
                part.Text = importedText
            );
        }

        protected override void Exporting(BodySummaryPart part, ExportContentContext context) {
            context.Element(part.PartDefinition.Name).SetAttributeValue("Text", part.Text);
        }

        private static BodySummaryEditorViewModel BuildEditorViewModel(BodySummaryPart part,RequestContext requestContext) {
            return new BodySummaryEditorViewModel {
                BodySummaryPart = part,
                EditorFlavor = GetFlavor(part),
                AddMediaPath = new PathBuilder(part,requestContext).AddContentType().AddContainerSlug().ToString()
            };
        }

        private static string GetFlavor(BodySummaryPart part) {
            var typePartSettings = part.Settings.GetModel<BodySummaryTypePartSettings>();
            return (typePartSettings != null && !string.IsNullOrWhiteSpace(typePartSettings.Flavor))
                       ? typePartSettings.Flavor
                       : part.PartDefinition.Settings.GetModel<BodySummaryPartSettings>().FlavorDefault;
        }

        class PathBuilder {
            private readonly IContent _content;
            private string _path;
            private readonly RequestContext _requestContext;

            public PathBuilder(IContent content,RequestContext requestContext) {
                _content = content;
                _path = "";
                _requestContext = requestContext;
            }

            public override string ToString() {
                return _path;
            }

            public PathBuilder AddContentType() {
                Add(_content.ContentItem.ContentType);
                return this;
            }

            public PathBuilder AddContainerSlug() {
                var common = _content.As<ICommonPart>();
                if (common == null || common.Container==null)
                    return this;
                var helper = new UrlHelper(_requestContext);
                Add(helper.ItemDisplayUrl(common.Container));
                return this;
            }

            private void Add(string segment) {
                if (string.IsNullOrEmpty(segment))
                    return;

                if (string.IsNullOrEmpty(_path)) {
                    _path = segment;
                }
                else if (segment.StartsWith("/")) {
                    _path = _path + segment;
                }
                else {
                    _path = _path + "/" + segment;
                }
            }
        }
    }
}