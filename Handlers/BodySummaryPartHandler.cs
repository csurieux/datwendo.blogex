using Datwendo.BlogEx.Models;
using Orchard.Data;
using Orchard.ContentManagement.Handlers;

namespace Datwendo.BlogEx.Handlers {
    public class BodySummaryPartHandler : ContentHandler {       
        public BodySummaryPartHandler(IRepository<BodySummaryPartRecord> bodyRepository) {
            Filters.Add(StorageFilter.For(bodyRepository));

            OnIndexing<BodySummaryPart>((context, BodySummaryPart) => context.DocumentIndex
                                                                .Add("bodysummary", BodySummaryPart.Record.Text).RemoveTags().Analyze()
                                                                .Add("format", BodySummaryPart.Record.Format).Store());
        }
    }
}