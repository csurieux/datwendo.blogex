﻿using Datwendo.BlogEx.Models;

namespace Datwendo.BlogEx.ViewModels {
    public class BodySummaryEditorViewModel {
        public BodySummaryPart BodySummaryPart { get; set; }

        public string Text {
            get { return BodySummaryPart.Text; }
            set { BodySummaryPart.Text = value; }
        }

        public string Format {
            get { return BodySummaryPart.Format; }
            set { BodySummaryPart.Format = value; }
        }

        public string EditorFlavor { get; set; }
        public string AddMediaPath { get; set; }
    }
}