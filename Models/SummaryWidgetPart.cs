﻿using Orchard.ContentManagement;

namespace Datwendo.BlogEx.Models
{
    public class SummaryWidgetPart : ContentPart
    {
        public int QueryId
        {
            get { return this.Retrieve(x => x.QueryId); }
            set { this.Store(x => x.QueryId, value); }
        }
    }
}