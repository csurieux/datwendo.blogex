using Orchard.ContentManagement;

namespace Datwendo.BlogEx.Models {
    public class BodySummaryPart : ContentPart<BodySummaryPartRecord> {
        public string Text {
            get { return Retrieve(x => x.Text); }
            set { Store(x => x.Text, value); }
        }

        public string Format {
            get { return Retrieve(x => x.Format); }
            set { Store(x => x.Format, value); }
        }
    }
}
