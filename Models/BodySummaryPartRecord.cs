using Orchard.ContentManagement.Records;
using Orchard.Data.Conventions;

namespace Datwendo.BlogEx.Models {
    public class BodySummaryPartRecord : ContentPartVersionRecord {
        [StringLengthMax]
        public virtual string Text { get; set; }

        public virtual string Format { get; set; }
    }
}