using Orchard;
using Orchard.ContentManagement;

namespace Datwendo.BlogEx.Services {
    public interface IHtmlFilterEx : IDependency {
        string ProcessContent(string text, string flavor, ContentItem Item);
    }
}