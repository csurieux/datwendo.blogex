﻿using System;
using Orchard.Environment.Extensions;
using Orchard.Tokens;
using System.Collections.Generic;
using Orchard.Services;
using Orchard.ContentManagement;

namespace Datwendo.BlogEx.Services {
    public class TokensFilter : IHtmlFilterEx
    {

        private readonly ITokenizer _tokenizer;
 
        public TokensFilter(ITokenizer tokenizer) {
            _tokenizer = tokenizer;
        }

        public string ProcessContent(string text, string flavor, ContentItem Item) {
            if (String.IsNullOrEmpty(text))
                return "";

            if (!text.Contains("#{")) {
                return text;
            }
            
            text = _tokenizer.Replace(text, new { Text = "Content", Content = Item }, new ReplaceOptions { Encoding = ReplaceOptions.NoEncode });

            return text;
        }
    }
}